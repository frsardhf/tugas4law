from fastapi import FastAPI, Header, status, Form
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional

app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8080",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Database
user_collection = {}

# Function to add user to collection
def insert_item(name: Optional[str] = None, npm: Optional[str] = None):
    user_collection[npm] = name

# Function to update user to collection
def update_item(name: Optional[str] = None, npm: Optional[str] = None):
    user_collection.update({npm: name})

# Function to retrieve item from collection
def get_specific_item(npm: Optional[str] = None):
    if npm in user_collection:
        value = user_collection[npm]
        return (npm, value)
    else:
        return None

# Service to update npm and name
async def update_npm(name, npm):
    data = get_specific_item(npm)
    print(data)

    if data == None:
        insert_item(name, npm)
    else:
        update_item(name, npm)

    return {'status': 'OK'}

async def read_npm(npm):
    content = ''

    data = get_specific_item(npm)
    print(data)

    if data == None:
        content = {
            'Error': 'Invalid request',
            'Description': "User does not exist"
        }
    else:
        content = {
            'Status': 'OK',
            'npm': data[0],
            'nama': data[1]
        }

    return content

# Controller
@app.post("/update")
async def update(name=Form(...), npm=Form(...)):
    response = await update_npm(name, npm)
    print(user_collection)
    return response

@app.get("/read/{npm}")
async def read(npm: str):
    response = await read_npm(npm)
    print(user_collection)
    return response
