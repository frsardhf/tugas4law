## Installation
Windows:
```shell
python -m venv env
env\Scripts\activate
pip install -r requirements.txt
```

Run:
```
uvicorn main:app --reload
```

## Documentation
```
open swagger docs in http://127.0.0.1:8000/docs
or click the link after running uvicorn command
```